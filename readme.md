# Broadcast application prototype

## Description

Based on [Node.js][NODEJS-LINK] and [Nest][NESTJS-LINK] framework

## Installation

### Docker
* Install [docker][DOCKER-LINK] / [docker-compose][DOCKERCOMPOSE-LINK].
* To use docker-container without switching to superuser mode, you need to add a group for the current system user: `sudo usermod -aG docker $USER`. Otherwise, you will have to run all the docker commands through the `sudo` command
* Install node dependencies:
  ```
  $ docker-compose run --rm app npm install
  ```
* Run application
  ```
  $ docker-compose up -d
  ```

## Console routines

* Install node dependencies:
  ```
  npm install
  ```
* Run application
  ```
  npm run start:dev
  ```

### Development Urls
* http://localhost:3000/send - Send broadcasting logout message
* http://localhost:3000/send/{token} - Send broadcasting logout message with user tokenized key
* http://localhost:3000/send/{token}/directly - Send logout message directly to unsubscribed user
* Local file ["client/index.html"](./client/index.html) - WebSocket client

[DOCKER-LINK]: https://docs.docker.com/install/
[DOCKERCOMPOSE-LINK]: https://docs.docker.com/compose/install/
[NODEJS-LINK]: https://nodejs.org
[NESTJS-LINK]: https://github.com/nestjs/nest
