export interface IEventsGateway {
    send(event: string, ...args: any[]): void;

    sendDirectly(token: string, event: string, ...args: any[]): void;
}