import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { IEventsGateway } from "./events.gateway.interface";

@WebSocketGateway()
export class EventsGateway implements IEventsGateway {
  @WebSocketServer()
  server: Server;

  send(event: string, ...args: any[]): void {
    this.server.emit(event, ...args);
  }

  sendDirectly(token: string, event: string, ...args: any[]): void {
    this.findClientsByToken(token)
      .forEach((client: Socket) => { client.emit(event, ...args); });
  }

  @SubscribeMessage('events')
  findAll(@MessageBody() data: string): string {
    return data;
  }

  @SubscribeMessage('identity')
  async identity(@MessageBody() data: number): Promise<number> {
    return data;
  }

  private findClientsByToken(token: string): Array<Socket> {
    return Object.values(this.server.clients().connected)
        .filter((client: Socket) => client.handshake.query.token === token);
  }
}
