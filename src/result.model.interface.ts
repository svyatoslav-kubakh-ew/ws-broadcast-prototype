export interface IResultModel {
    event: string;
    status: string;
    token?: string;
}