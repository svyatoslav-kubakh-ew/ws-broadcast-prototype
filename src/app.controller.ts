import { Controller, Get, Param } from '@nestjs/common';
import { IResultModel } from "./result.model.interface";
import {
  EventsGateway,
  IEventsGateway
} from "./events";

@Controller('send')
export class AppController {
  private readonly events: IEventsGateway;

  constructor(
      eventsGateway: EventsGateway,
  ) {
    this.events = eventsGateway;
  }

  @Get()
  async sendBroadcast(): Promise<IResultModel> {
    await this.events.send('logoutAll', {someAttr: 1});
    return {event: 'logoutAll', status: 'ok'};
  }

  @Get(':token')
  sendEventToUser(@Param('token') token): IResultModel {
    this.events.send('logout-' + token, {someAttr: 2, someAttr2: 3});
    return {event: 'logout', token, status: 'ok'};
  }

  @Get(':token/directly')
  async sendToUser(@Param('token') token): Promise<IResultModel> {
    await this.events.sendDirectly(token, 'logoutDirectly', {someAttr: 4, someAttr2: 5, someAttr3: 6});
    return {event: 'logoutDirectly', token, status: 'ok'};
  }
}
