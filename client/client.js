function log(message) {
    document.write(`[${new Date().toLocaleTimeString()}] ${message}<br/>`);
}

const token = Math.random().toString(36).substring(7);

const socket = io('http://localhost:3000', {query: {token}});

socket.on('connect', () => {
    log('<b>Connected</b> with token <u>' + token + '</u>');
});
socket.on('logoutAll', (data) => {
    log('<b>Logout notification received</b> (all users): ' + JSON.stringify(data));
});
socket.on('logout-' + token, (data) => {
    log('<b>Logout notification received</b> (by ID): ' + JSON.stringify(data));
});
socket.on('logoutDirectly', (data) => {
    log('<b>Logout notification received</b> (directly): ' + JSON.stringify(data));
});
socket.on('disconnect', function() {
    log('<b>Disconnected</b>');
});
